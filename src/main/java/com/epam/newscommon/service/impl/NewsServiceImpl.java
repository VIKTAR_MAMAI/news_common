package com.epam.newscommon.service.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.valueobject.NewsPageItem;
import com.epam.newscommon.valueobject.NewsSearchCriteria;

@Service("newsService")
public class NewsServiceImpl implements INewsService {
	@Autowired
	private NewsDao newsDao;

	@Override
	public List<NewsEntity> loadAll() throws ServiceException {
		try {
			List<NewsEntity> newsList = newsDao.loadAll();
			return newsList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public NewsEntity loadById(Long id) throws ServiceException {
		try {
			NewsEntity newsEntity = newsDao.loadById(id);
			return newsEntity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public Long loadNextId(NewsSearchCriteria filteredItem, Long id) throws ServiceException {
		try {
			return newsDao.loadNextId(filteredItem, id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long loadPreviousId(NewsSearchCriteria filteredItem, Long id) throws ServiceException {
		try {
			return newsDao.loadPreviousId(filteredItem, id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void attachTags(Long newsId, List<Long> tagId) throws ServiceException {
		try {
			if (tagId == null || tagId.isEmpty()) {
				return;
			}
			tagId.removeAll(Collections.singleton(null));
			newsDao.attachTags(newsId, tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void attachAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDao.attachAuthor(newsId, authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(NewsEntity entity) throws ServiceException {
		try {
			Long id = newsDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long update(NewsEntity entity) throws ServiceException {
		try {
			Long id = newsDao.update(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void deleteList(List<Long> newsIdList) throws ServiceException {
		if (newsIdList == null) {
			throw new ServiceException("No one news was selected to delete!");
		}
		try {
			newsDao.deleteList(newsIdList);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void detachTags(Long newsId) throws ServiceException {
		try {
			newsDao.detachTags(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDao.updateAuthor(newsId, authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public NewsPageItem<NewsEntity> loadByFilter(NewsSearchCriteria filteredItem, Integer pageNumber,
			int newsPerPage) throws ServiceException {
		if (pageNumber == null) {
			pageNumber = 1;
		}
		try {
			return newsDao.loadByFilter(filteredItem, pageNumber, newsPerPage);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
