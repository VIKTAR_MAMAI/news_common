package com.epam.newscommon.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;
import com.epam.newscommon.service.ICommentService;
import com.epam.newscommon.service.INewsManagementService;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.service.ITagService;
import com.epam.newscommon.valueobject.NewsPageItem;
import com.epam.newscommon.valueobject.NewsSearchCriteria;
import com.epam.newscommon.valueobject.NewsVO;

@Service("newsManageService")
public class NewsManagementServiceImpl implements INewsManagementService {

	@Autowired
	private INewsService newsService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private ICommentService commentService;

	@Transactional
	@Override
	public List<NewsVO> loadAll() throws ServiceException {
		List<NewsEntity> newsList = newsService.loadAll();
		List<NewsVO> newsWrapperList = constructVOlist(newsList);
		return newsWrapperList;
	}

	@Transactional
	@Override
	public NewsVO loadById(Long newsId) throws ServiceException {
		NewsEntity newsEntity = newsService.loadById(newsId);
		if (newsEntity == null) {
			throw new ServiceException("News with such id does not exist");
		}
		NewsVO newsObject = constructValueObject(newsEntity);
		return newsObject;
	}

	@Transactional
	@Override
	public NewsPageItem<NewsVO> loadByFilter(NewsSearchCriteria filteredItem, Integer pageNumber,
			int newsPerPage) throws ServiceException {
		NewsPageItem<NewsEntity> item = newsService.loadByFilter(filteredItem, pageNumber, newsPerPage);
		NewsPageItem<NewsVO> resultItem = new NewsPageItem<NewsVO>(constructVOlist(item.getNewsList()),
				item.getPageNumber(), item.getPageCount());
		return resultItem;
	}

	@Transactional
	@Override
	public Long create(NewsEntity newsEntity, List<Long> tagIdList, Long authorId) throws ServiceException {
		Long newsId = newsService.create(newsEntity);
		newsService.attachAuthor(newsId, authorId);
		newsService.attachTags(newsId, tagIdList);
		return newsId;
	}

	@Transactional
	@Override
	public Long update(NewsEntity newsEntity, List<Long> tagIdList, Long authorId) throws ServiceException {
		Long newsId = newsService.update(newsEntity);
		newsService.updateAuthor(newsId, authorId);
		newsService.detachTags(newsId);
		if (tagIdList != null) {
			newsService.attachTags(newsId, tagIdList);
		}
		return newsId;
	}

	private List<NewsVO> constructVOlist(List<NewsEntity> newsList) throws ServiceException {
		List<NewsVO> newsWrapperList = new LinkedList<NewsVO>();
		for (NewsEntity entity : newsList) {
			NewsVO newsObject = constructValueObject(entity);
			newsWrapperList.add(newsObject);
		}
		return newsWrapperList;
	}

	private NewsVO constructValueObject(NewsEntity entity) throws ServiceException {
		Long newsId = entity.getId();
		List<TagEntity> tagList = tagService.loadByNewsId(newsId);
		AuthorEntity author = authorService.loadByNewsId(newsId);
		List<CommentEntity> commentList = commentService.loadByNewsId(newsId);
		NewsVO newsObject = new NewsVO(entity, tagList, author, commentList);
		return newsObject;
	}
}
