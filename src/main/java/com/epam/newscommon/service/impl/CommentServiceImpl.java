package com.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.ICommentService;

@Service("commentService")
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private CommentDao commentDao;

	@Override
	public List<CommentEntity> loadAll() throws ServiceException {
		try {
			List<CommentEntity> commentList = commentDao.loadAll();
			return commentList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public CommentEntity loadById(Long id) throws ServiceException {
		try {
			CommentEntity entity = commentDao.loadById(id);
			return entity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<CommentEntity> loadByNewsId(Long newsId)
			throws ServiceException {
		try {
			List<CommentEntity> commentList = commentDao.loadByNewsId(newsId);
			return commentList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(CommentEntity entity) throws ServiceException {
		try {
			Long id = commentDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long update(CommentEntity entity) throws ServiceException {
		try {
			Long id = commentDao.update(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
