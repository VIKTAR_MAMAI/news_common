package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.NewsPageItem;
import com.epam.newscommon.valueobject.NewsSearchCriteria;
import com.epam.newscommon.valueobject.NewsVO;

public interface INewsManagementService {

	/**
	 * @param pageNumber
	 *            - number of page for pagination usage
	 * @return - list of news entities for appropriate page
	 * @throws ServiceException
	 * 
	 */
	List<NewsVO> loadAll() throws ServiceException;
	
	/**
	 * @param newsId
	 *            - unique news identifier
	 * @return - value object which contains news entity with matching author,
	 *         list of tags, and list of comments
	 * @throws ServiceException
	 */
	NewsVO loadById(Long newsId) throws ServiceException;

	NewsPageItem<NewsVO> loadByFilter(NewsSearchCriteria filteredItem, Integer pageNumber, int newsPerPage) throws ServiceException;

	/**
	 * 
	 * @param newsEntity
	 * @see NewsEntity
	 * @param tagIdList
	 *            - list of tags' id
	 * @param authorId
	 *            - unique author identifier
	 * @return - identifier of inserted news entity
	 * @throws ServiceException
	 */
	Long create(NewsEntity newsEntity, List<Long> tagIdList, Long authorId) throws ServiceException;

	/**
	 * 
	 * @param newsEntity
	 * @see NewsEntity
	 * @param tagIdList
	 *            - list of tags' id
	 * @param authorId
	 *            - unique author entity identifier
	 * @return - identifier of updated news entity
	 * @throws ServiceException
	 */
	Long update(NewsEntity newsEntity, List<Long> tagIdList, Long authorId) throws ServiceException;

}
