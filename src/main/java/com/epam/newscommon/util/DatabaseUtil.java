package com.epam.newscommon.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class DatabaseUtil {
	private static final Logger LOG = Logger.getLogger(DatabaseUtil.class);

	/**
	 * <p>
	 * method that helps our dao layer closing connection and statement
	 * 
	 * @param connection
	 *            - opened connection
	 * @param statement
	 *            - opened statement
	 */
	public static void close(DataSource dataSource, Connection connection, Statement statement) {

		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			LOG.error(e);
		}

		if (connection != null) {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * <p>
	 * method that helps our dao layer closing connection and statement
	 * 
	 * @param connection
	 *            - opened connection
	 * @param statement
	 *            - opened statement
	 * @param rs
	 *            - opened resultset
	 */
	public static void close(DataSource dataSource, Connection connection, Statement statement,
			ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		close(dataSource, connection, statement);
	}

}
