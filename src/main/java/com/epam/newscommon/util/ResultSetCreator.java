package com.epam.newscommon.util;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.newscommon.constant.SchemaSQL;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

/**
 * 
 * utility class that transforms object stored in database into entity, by
 * extracting appropriate fields
 * 
 */
public class ResultSetCreator {
	public static NewsEntity createNews(ResultSet rs) throws DaoException {
		try {
			Long id = rs.getLong(SchemaSQL.NEWS_ID);
			String shortText = rs.getString(SchemaSQL.NEWS_SHORT_TEXT);
			String fullText = rs.getString(SchemaSQL.NEWS_FULL_TEXT);
			String title = rs.getString(SchemaSQL.NEWS_TITLE);
			Date creationDate = rs.getDate(SchemaSQL.NEWS_CREATION_DATE);
			Date modificationDate = rs.getDate(SchemaSQL.NEWS_MODIFICATION_DATE);
			NewsEntity entity = new NewsEntity(id, shortText, fullText, title, creationDate);
			entity.setModificationDate(modificationDate);
			return entity;
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	public static TagEntity createTag(ResultSet rs) throws DaoException {
		try {
			Long id = rs.getLong(SchemaSQL.TAG_ID);
			String name = rs.getString(SchemaSQL.TAG_NAME);
			TagEntity entity = new TagEntity(id, name);
			return entity;
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	public static AuthorEntity createAuthor(ResultSet rs) throws DaoException {
		try {
			Long id = rs.getLong(SchemaSQL.AUTHOR_ID);
			String name = rs.getString(SchemaSQL.AUTHOR_NAME);
			Date expireddate = rs.getDate(SchemaSQL.AUTHOR_EXPIRED);
			AuthorEntity entity = new AuthorEntity(id, name);
			entity.setExpiredDate(expireddate);
			return entity;
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	public static CommentEntity createComment(ResultSet rs) throws DaoException {
		try {
			Long id = rs.getLong(SchemaSQL.COMMENTS_ID);
			String text = rs.getString(SchemaSQL.COMMENT_TEXT);
			Date creationDate = rs.getDate(SchemaSQL.COMMENT_CREATION_DATE);
			Long newsId = rs.getLong(SchemaSQL.NEWS_ID);
			CommentEntity entity = new CommentEntity(id, text, newsId);
			entity.setCreationDate(creationDate);
			return entity;
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	/**
	 * <p>
	 * 
	 * @param rs
	 *            - {@link ResultSet}
	 * @param NEWS_PER_PAGE
	 *            - count of news to be viewed on list page
	 * @return - total count of pages
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	public static Integer calculatePageCount(ResultSet rs, final int NEWS_PER_PAGE)
			throws DaoException {
		try {
			Integer totalCount = rs.getInt(SchemaSQL.TOTAL_COUNT);
			Integer pageCount = null;
			if (totalCount != null) {
				pageCount = (int) Math.ceil(totalCount * 1.0 / NEWS_PER_PAGE);
			}
			return pageCount;
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}
}
