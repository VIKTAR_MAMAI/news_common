package com.epam.newscommon.util;

import java.util.List;

public class QueryHelper {
	
	public static String convertListToString(List<Long> tagIdList) {
		StringBuilder builder = new StringBuilder();
		int size = tagIdList.size();
		for (int i = 0; i < size; i++) {
			builder.append(tagIdList.get(i));
			if (i < size - 1) {
				builder.append(", ");
			}
		}
		return builder.toString();
	}

	public static String wrapQueryToRownum(String query) {
		String begin = "SELECT * FROM ( SELECT n.*, ROWNUM rn, count(*) OVER () total_count FROM (";
		String end = ") n ) WHERE rn BETWEEN ? AND ?";
		StringBuilder builder = new StringBuilder();
		builder.append(begin).append(query).append(end);
		return builder.toString();
	}

}
