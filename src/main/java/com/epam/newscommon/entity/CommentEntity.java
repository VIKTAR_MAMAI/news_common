package com.epam.newscommon.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * mapped with table comments
 * 
 */

public class CommentEntity implements Serializable {
	private static final long serialVersionUID = -6066984816756385932L;
	
	private Long id;
	private String text;
	private Date creationDate;
	private Long newsId;

	public CommentEntity() {
	}

	public CommentEntity(Long id, String text, Long newsId) {
		this.id = id;
		this.text = text;
		this.newsId = newsId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentEntity other = (CommentEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentEntity [id=" + id + ", text=" + text + ", creationDate=" + creationDate
				+ ", newsId=" + newsId + "]";
	}

}
