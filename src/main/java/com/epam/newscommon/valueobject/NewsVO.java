package com.epam.newscommon.valueobject;

import java.util.Collections;
import java.util.List;

import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;

/**
 * entity that wrappes news entity and associated authors, tags, comments
 */
public class NewsVO {

	private final NewsEntity newsEntity;
	private final List<TagEntity> tagList;
	private final AuthorEntity authorEntity;
	private final List<CommentEntity> commentList;

	public NewsVO(NewsEntity newsEntity, List<TagEntity> tagList, AuthorEntity authorEntity,
			List<CommentEntity> commentList) {
		this.newsEntity = newsEntity;
		this.tagList = tagList;
		this.authorEntity = authorEntity;
		this.commentList = commentList;
	}

	public NewsEntity getNewsEntity() {
		return newsEntity;
	}

	public List<TagEntity> getTagList() {
		return Collections.unmodifiableList(tagList);
	}

	public AuthorEntity getAuthorEntity() {
		return authorEntity;
	}

	public List<CommentEntity> getCommentList() {
		return Collections.unmodifiableList(commentList);
	}

}
