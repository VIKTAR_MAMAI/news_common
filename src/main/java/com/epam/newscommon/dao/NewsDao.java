package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.valueobject.NewsPageItem;
import com.epam.newscommon.valueobject.NewsSearchCriteria;

/**
 * 
 * interacts with table news
 * 
 */
public interface NewsDao extends CommonDao<Long, NewsEntity> {

	NewsPageItem<NewsEntity> loadByFilter(NewsSearchCriteria filteredItem, Integer pageNumber, int newsPerPage)
			throws DaoException;

	Long loadNextId(NewsSearchCriteria filteredItem, Long id) throws DaoException;

	Long loadPreviousId(NewsSearchCriteria filteredItem, Long id) throws DaoException;

	/**
	 * <p>
	 * inserts data in intermediate table news_tag
	 * 
	 * @param newsId
	 *            - news identifier
	 * @param tagId
	 *            - tag identifier
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void attachTags(Long newsId, List<Long> tagIdList) throws DaoException;

	/**
	 * <p>
	 * inserts data in intermediate table news_author
	 * 
	 * @param newsId
	 *            - news identifier
	 * @param authorId
	 *            - author identifier
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void attachAuthor(Long newsId, Long authorId) throws DaoException;

	/**
	 * 
	 * @param newsId
	 *            - unique news identifier
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void detachTags(Long newsId) throws DaoException;

	/**
	 * 
	 * @param newsId
	 *            - unique news identifier
	 * @param authorId
	 *            - unique author identifier
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void updateAuthor(Long newsId, Long authorId) throws DaoException;

	/**
	 * 
	 * @param newsIdList
	 *            - array of unique news identifiers
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void deleteList(List<Long> newsIdList) throws DaoException;
}