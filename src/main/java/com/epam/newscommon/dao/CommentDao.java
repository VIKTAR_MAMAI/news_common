package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.DaoException;

/**
 * 
 * interacts with table comments
 */
public interface CommentDao extends CommonDao<Long, CommentEntity> {
	/**
	 * 
	 * @param newsId
	 *            - unique news identifier
	 * @return - list of comments each of one has such newsId
	 * @throws DaoException
	 *             - if there is any connection error
	 */
	List<CommentEntity> loadByNewsId(Long newsId) throws DaoException;
}
