package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

/**
 * 
 * interacts with table tag
 */
public interface TagDao extends CommonDao<Long, TagEntity> {
	/**
	 * 
	 * @param newsId
	 *            - unique news identifier
	 * @return - list of tags each of one has such newsId
	 * @throws DaoException
	 *             - if there is any connection error
	 */
	List<TagEntity> loadByNewsId(Long newsId) throws DaoException;
}
