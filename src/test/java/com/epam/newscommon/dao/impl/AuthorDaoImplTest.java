package com.epam.newscommon.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContextTest.xml")
@TestExecutionListeners(listeners = { DbUnitTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class })
@DatabaseSetup(value = { "/dbunit/author-data.xml", "/dbunit/news_author-data.xml" })
public class AuthorDaoImplTest {

	@Autowired
	private AuthorDao authorDao;

	@Test
	public void loadAll() throws DaoException {
		List<AuthorEntity> authorList = authorDao.loadAll();
		assertEquals(20, authorList.size());
	}
	
	@Test
	public void loadActiveAuthors() throws DaoException {
		List<AuthorEntity> authorList = authorDao.loadActiveAuthors();
		assertEquals(20, authorList.size());
	}

	@Test
	public void loadByNewsId() throws DaoException {
		AuthorEntity actualEntity = authorDao.loadByNewsId(Long.valueOf(100));
		assertNull(actualEntity);
	}

	@Test
	public void loadById() throws DaoException {
		AuthorEntity entity = authorDao.loadById(Long.valueOf(100));
		assertNull(entity);
	}

	@Test
	public void create() throws DaoException {
		AuthorEntity entity = new AuthorEntity();
		entity.setName("valera");
		authorDao.create(entity);
		entity = new AuthorEntity(Long.valueOf(100), "gilbert");
		authorDao.create(entity);
		List<AuthorEntity> authorList = authorDao.loadAll();
		assertEquals(22, authorList.size());
	}

	@Test
	public void update() throws DaoException {
		AuthorEntity entity = new AuthorEntity(Long.valueOf(6), "gilbert");
		authorDao.update(entity);
		AuthorEntity actualEntity = authorDao.loadById(Long.valueOf(6));
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void delete() throws DaoException {
		Long id = Long.valueOf(6);
		authorDao.delete(id);

		AuthorEntity author = authorDao.loadById(id);
		assertNull(author);
	}
	
	@Test
	public void makeExpired() throws DaoException {
		Long id = Long.valueOf(6);
		authorDao.makeExpired(id);

		AuthorEntity author = authorDao.loadById(id);
		assertNotNull(author.getExpiredDate());
	}

	private void assertEntityEquals(AuthorEntity expected, AuthorEntity actual) {
		assertEquals(expected.getName(), actual.getName());
	}
}
