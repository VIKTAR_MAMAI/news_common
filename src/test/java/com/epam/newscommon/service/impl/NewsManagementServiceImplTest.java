package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;
import com.epam.newscommon.service.ICommentService;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.service.ITagService;
import com.epam.newscommon.valueobject.NewsVO;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceImplTest {

	@InjectMocks
	private static NewsManagementServiceImpl newsManageService;

	@Mock
	private INewsService newsService;

	@Mock
	private IAuthorService authorService;

	@Mock
	private ITagService tagService;

	@Mock
	private ICommentService commentService;

	@Test
	public void loadAll() throws ServiceException, DaoException {
		when(newsService.loadAll()).thenReturn(generateNewsList());
		List<NewsVO> newsList = newsManageService.loadAll();
		assertEquals(3, newsList.size());
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		Long newsId = Long.valueOf(1);
		NewsVO newsObject = generateValueObject(newsId);
		when(newsService.loadById(newsId)).thenReturn(newsObject.getNewsEntity());
		when(authorService.loadByNewsId(newsId)).thenReturn(newsObject.getAuthorEntity());
		when(tagService.loadByNewsId(newsId)).thenReturn(newsObject.getTagList());
		when(commentService.loadByNewsId(newsId)).thenReturn(newsObject.getCommentList());
		NewsVO actualObject = newsManageService.loadById(newsId);
		assertEntityEquals(newsObject, actualObject);
	}
	
	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		newsManageService.loadById(null);
	}

	@Test
	public void create() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(7);
		Long newsId = Long.valueOf(1);
		List<Long> tagIdList = Arrays.asList((long) 4, (long) 5);
		NewsEntity entity = new NewsEntity(Long.valueOf(1), "liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date.valueOf("2004-12-01"));

		doReturn(newsId).when(newsService).create(entity);
		newsManageService.create(entity, tagIdList, authorId);
		verify(newsService).attachAuthor(newsId, authorId);
		verify(newsService).attachTags(newsId, tagIdList);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(7);
		Long newsId = Long.valueOf(1);
		List<Long> tagIdList = Arrays.asList((long) 4, (long) 5);
		NewsEntity entity = new NewsEntity(Long.valueOf(1), "liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date.valueOf("2004-12-01"));

		doReturn(newsId).when(newsService).update(entity);
		newsManageService.update(entity, tagIdList, authorId);
		verify(newsService).updateAuthor(newsId, authorId);
		verify(newsService).detachTags(newsId);
		verify(newsService).attachTags(newsId, tagIdList);
	}

	@Test(expected = ServiceException.class)
	public void createFail() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(7);
		Long newsId = Long.valueOf(1);
		List<Long> tagIdList = Arrays.asList((long) 4, (long) 5);
		NewsEntity entity = new NewsEntity(Long.valueOf(1), "liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date.valueOf("2004-12-01"));

		doReturn(newsId).when(newsService).create(entity);
		doThrow(ServiceException.class).when(newsService).attachAuthor(newsId, authorId);
		newsManageService.create(entity, tagIdList, authorId);
	}

	private NewsVO generateValueObject(Long newsId) {
		NewsVO newsObject = new NewsVO(new NewsEntity(Long.valueOf(newsId),
				"liverpool with arsenal", "liverpool plays with arsenal in quarters", "football",
				Date.valueOf("2012-03-14")), generateTagList(), new AuthorEntity(Long.valueOf(2),
				"kitaev"), generateCommentList(newsId));
		return newsObject;
	}

	private List<TagEntity> generateTagList() {
		List<TagEntity> tagList = new ArrayList<TagEntity>() {
			{
				this.add(new TagEntity(Long.valueOf(5), "sun"));
				this.add(new TagEntity(Long.valueOf(7), "rain"));
				this.add(new TagEntity(Long.valueOf(9), "thunder"));
			}
		};
		return tagList;
	}

	private List<NewsEntity> generateNewsList() {
		List<NewsEntity> newsList = new ArrayList<NewsEntity>();
		newsList.add(new NewsEntity(Long.valueOf(1), "liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date.valueOf("2012-03-14")));
		newsList.add(new NewsEntity(Long.valueOf(2), "scotland and italy",
				"scotland plays with italy in final", "rugby", Date.valueOf("2012-03-14")));
		newsList.add(new NewsEntity(Long.valueOf(3), "major resign in russia",
				"russian chief athletics resigns", "athletics", Date.valueOf("2012-03-14")));
		return newsList;
	}

	private List<CommentEntity> generateCommentList(final Long newsId) {
		List<CommentEntity> commentList = new ArrayList<CommentEntity>() {
			{
				this.add(new CommentEntity(Long.valueOf(2), "satisfied!!!", newsId));
				this.add(new CommentEntity(Long.valueOf(4), "best team", newsId));
				this.add(new CommentEntity(Long.valueOf(6), "fantastic game", newsId));
			}
		};
		return commentList;
	}

	private void assertEntityEquals(NewsVO newsObject, NewsVO actualObject) {
	}
}
