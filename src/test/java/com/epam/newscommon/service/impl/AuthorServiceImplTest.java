package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.impl.AuthorDaoImpl;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
	@InjectMocks
	private AuthorServiceImpl authorService;

	@Mock
	private AuthorDaoImpl authorDao;

	private ArrayList<AuthorEntity> authorList;

	@Before
	public void setUp() {
		authorList = new ArrayList<AuthorEntity>() {
			{
				this.add(new AuthorEntity(Long.valueOf(2), "kitaev"));
				this.add(new AuthorEntity(Long.valueOf(4), "molenkov"));
				this.add(new AuthorEntity(Long.valueOf(6), "ruslanov"));
			}
		};
	}

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(authorList).when(authorDao).loadAll();
		List<AuthorEntity> actualTagList = authorService.loadAll();
		verify(authorDao, times(1)).loadAll();
		assertEquals(authorList.size(), actualTagList.size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).loadAll();
		authorService.loadAll();
	}

	@Test
	public void loadActiveAuthors() throws ServiceException, DaoException {
		doReturn(authorList).when(authorDao).loadActiveAuthors();
		List<AuthorEntity> actualTagList = authorService.loadActiveAuthors();
		verify(authorDao, times(1)).loadActiveAuthors();
		assertEquals(authorList.size(), actualTagList.size());
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity();
		entity.setName("molenkov");
		doReturn(entity).when(authorDao).loadById(Long.valueOf(4));
		AuthorEntity actualEntity = authorService.loadById(Long.valueOf(4));
		verify(authorDao, times(1)).loadById(anyLong());
		assertEntityEquals(actualEntity, entity);
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).loadById(anyLong());
		authorService.loadById(Long.valueOf(4));
	}

	@Test
	public void loadByNewsId() throws ServiceException, DaoException {
		AuthorEntity entity = authorList.get(1);
		doReturn(entity).when(authorDao).loadByNewsId(Long.valueOf(7));
		AuthorEntity actualEntity = authorService.loadByNewsId(Long.valueOf(7));
		verify(authorDao, atLeast(1)).loadByNewsId(anyLong());
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void create() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity();
		entity.setName("petrenko");
		Long id = authorService.create(entity);
		assertNotNull(id);
	}
	
	@Test(expected = ServiceException.class)
	public void createFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).create(any(AuthorEntity.class));
		AuthorEntity entity = new AuthorEntity();
		entity.setName("petrenko");
		authorService.create(entity);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity(Long.valueOf(3), "stepanov");
		Long id = authorService.update(entity);
		assertNotNull(id);
		verify(authorDao, atLeastOnce()).update(any(AuthorEntity.class));
	}

	@Test
	public void delete() throws ServiceException, DaoException {
		authorService.delete(Long.valueOf(2));
		authorService.delete(Long.valueOf(4));
		verify(authorDao, times(2)).delete(anyLong());
	}
	
	@Test(expected = ServiceException.class)
	public void deleteFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).delete(anyLong());
		authorService.delete(Long.valueOf(4));
	}

	@Test
	public void makeExpired() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(2);
		authorService.makeExpired(authorId);
		verify(authorDao).makeExpired(authorId);
	}

	private void assertEntityEquals(AuthorEntity entity, AuthorEntity actualEntity) {
		assertEquals(entity.getName(), actualEntity.getName());
	}
}
