package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.impl.NewsDaoImpl;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.NewsSearchCriteria;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	@InjectMocks
	private static NewsServiceImpl newsService;

	@Mock
	private NewsDaoImpl newsDao;

	private List<NewsEntity> newsList;

	@Before
	public void setUp() {
		newsList = generateList();
	}

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(newsList).when(newsDao).loadAll();

		assertNotNull(newsService.loadAll());
		assertEquals(3, newsService.loadAll().size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).loadAll();
		newsService.loadAll();
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		NewsEntity news1 = newsList.get(0);
		doReturn(news1).when(newsDao).loadById(Long.valueOf(1));
		NewsEntity news2 = newsList.get(2);
		doReturn(news2).when(newsDao).loadById(Long.valueOf(3));

		assertEquals(news1, newsService.loadById(Long.valueOf(1)));
		assertEquals(news2, newsService.loadById(Long.valueOf(3)));
		assertNull(newsService.loadById(Long.valueOf(2)));
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).loadById(anyLong());
		newsService.loadById(Long.valueOf(3));
	}

	@Test
	public void loadNextId() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] { 1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		NewsSearchCriteria filteredItem = new NewsSearchCriteria(tagIdList, authorId);
		newsService.loadNextId(filteredItem, newsId);
		verify(newsDao).loadNextId(filteredItem, newsId);
	}

	@Test(expected = ServiceException.class)
	public void loadNextIdFail() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] { 1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		NewsSearchCriteria filteredItem = new NewsSearchCriteria(tagIdList, authorId);
		doThrow(DaoException.class).when(newsDao).loadNextId(filteredItem, newsId);
		newsService.loadNextId(filteredItem, newsId);
	}

	@Test
	public void loadPreviousId() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] { 1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		NewsSearchCriteria filteredItem = new NewsSearchCriteria(tagIdList, authorId);
		newsService.loadPreviousId(filteredItem, newsId);
		verify(newsDao).loadPreviousId(filteredItem, newsId);
	}

	@Test(expected = ServiceException.class)
	public void loadPreviousIdFail() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] { 1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		NewsSearchCriteria filteredItem = new NewsSearchCriteria(tagIdList, authorId);
		doThrow(DaoException.class).when(newsDao).loadPreviousId(filteredItem, newsId);
		newsService.loadPreviousId(filteredItem, newsId);
	}

	@Test
	public void attachTags() throws ServiceException, DaoException {
		List<Long> tagidList = Arrays.asList((long) 3, (long) 8);
		newsService.attachTags(Long.valueOf(3), tagidList);

		verify(newsDao).attachTags(anyLong(), Mockito.anyListOf(Long.class));
	}

	@Test
	public void attachAuthor() throws ServiceException, DaoException {
		newsService.attachAuthor(Long.valueOf(3), Long.valueOf(7));

		verify(newsDao).attachAuthor(anyLong(), anyLong());
	}

	@Test
	public void create() throws ServiceException, DaoException {
		NewsEntity entity = new NewsEntity(
				Long.valueOf(50),
				"Recovery operation resumes",
				"A search and recovery operation has resumed in the southern French Alps after Tuesday's crash of a Germanwings plane with 150 people on board",
				"Air crash", Date.valueOf("2007-11-06"));
		Long id = newsService.create(entity);
		assertNotNull(id);
		verify(newsDao).create(entity);
	}

	@Test(expected = ServiceException.class)
	public void createFail() throws ServiceException, DaoException {
		NewsEntity entity = new NewsEntity(
				Long.valueOf(50),
				"Recovery operation resumes",
				"A search and recovery operation has resumed in the southern French Alps after Tuesday's crash of a Germanwings plane with 150 people on board",
				"Air crash", Date.valueOf("2007-11-06"));
		doThrow(DaoException.class).when(newsDao).create(any(NewsEntity.class));
		newsService.create(entity);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		NewsEntity news = newsList.get(1); // id 2
		news.setTitle("italy beats scotland surprisingly");

		doReturn(Long.valueOf(2)).when(newsDao).update(news);
		Long id = newsService.update(news);

		assertEquals(Long.valueOf(2), id);

	}

	@Test
	public void delete() throws ServiceException, DaoException {
		newsService.delete(Long.valueOf(5));
		newsService.delete(Long.valueOf(1));
		newsService.delete(Long.valueOf(102));

		verify(newsDao, times(3)).delete(anyLong());
	}

	@Test
	public void deleteList() throws ServiceException, DaoException {
		List<Long> newsIdList = Arrays.asList((long) 3, (long) 4);
		newsService.deleteList(newsIdList);
		verify(newsDao).deleteList(newsIdList);
	}

	@Test(expected = ServiceException.class)
	public void deleteListFailNull() throws ServiceException, DaoException {
		newsService.deleteList(null);
	}
	
	@Test(expected = ServiceException.class)
	public void deleteListFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).deleteList(Mockito.anyListOf(Long.class));
		List<Long> idList = Arrays.asList(ArrayUtils.toObject(new long[] { 1, 3, 5, 6 }));
		newsService.deleteList(idList);
	}

	@Test
	public void detachTags() throws ServiceException, DaoException {
		Long newsId = Long.valueOf(15);
		newsService.detachTags(newsId);
		verify(newsDao, times(1)).detachTags(newsId);
	}

	@Test
	public void updateAuthor() throws ServiceException, DaoException {
		Long newsId = Long.valueOf(15);
		Long authorId = Long.valueOf(3);
		newsService.updateAuthor(newsId, authorId);
		verify(newsDao, times(1)).updateAuthor(newsId, authorId);
	}

	private List<NewsEntity> generateList() {
		List<NewsEntity> newsList = new ArrayList<NewsEntity>();
		newsList.add(new NewsEntity(Long.valueOf(1), "liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date.valueOf("2004-12-01")));
		newsList.add(new NewsEntity(Long.valueOf(2), "scotland and italy", "scotland plays with italy in final",
				"rugby", Date.valueOf("2014-10-23")));
		newsList.add(new NewsEntity(Long.valueOf(3), "major resign in russia", "russian chief athletics resigns",
				"athletics", Date.valueOf("2014-09-25")));
		return newsList;
	}
}
