package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.impl.TagDaoImpl;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
	@InjectMocks
	private TagServiceImpl tagService;

	@Mock
	private TagDaoImpl tagDao;

	private ArrayList<TagEntity> tagList;

	@Before
	public void setUp() {
		tagList = new ArrayList<TagEntity>() {
			{
				this.add(new TagEntity(Long.valueOf(5), "sun"));
				this.add(new TagEntity(Long.valueOf(7), "rain"));
				this.add(new TagEntity(Long.valueOf(9), "thunder"));
			}
		};
	}

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(tagList).when(tagDao).loadAll();
		List<TagEntity> actualTagList = tagService.loadAll();
		verify(tagDao, times(1)).loadAll();
		assertEquals(tagList.size(), actualTagList.size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).loadAll();
		tagService.loadAll();
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		TagEntity entity = tagList.get(1); // id = 7
		doReturn(entity).when(tagDao).loadById(Long.valueOf(7));
		TagEntity actualEntity = tagService.loadById(Long.valueOf(7));
		verify(tagDao, times(1)).loadById(anyLong());
		assertEntityEquals(actualEntity, entity);
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).loadById(anyLong());
		tagService.loadById(Long.valueOf(4));
	}

	@Test
	public void loadByNewsId() throws ServiceException, DaoException {
		doReturn(tagList).when(tagDao).loadByNewsId(Long.valueOf(7));
		List<TagEntity> actualTagList = tagService.loadByNewsId(Long.valueOf(7));
		verify(tagDao, atLeast(1)).loadByNewsId(anyLong());
		assertEquals(tagList.size(), actualTagList.size());
	}

	@Test
	public void create() throws ServiceException, DaoException {
		doReturn(tagList).when(tagDao).loadAll();
		TagEntity entity = new TagEntity();
		entity.setName("color");
		Long id = tagService.create(entity);
		assertNotNull(id);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		verifyZeroInteractions(tagDao);
		TagEntity entity = new TagEntity(Long.valueOf(7), "color");
		Long id = tagService.update(entity);
		assertNotNull(id);
		verify(tagDao, atLeastOnce()).update(any(TagEntity.class));
	}
	
	@Test(expected = ServiceException.class)
	public void updateFail() throws ServiceException, DaoException {
		TagEntity entity = new TagEntity(Long.valueOf(101), "sunshine");
		doThrow(DaoException.class).when(tagDao).update(entity);
		tagService.update(entity);
	}

	@Test
	public void delete() throws ServiceException, DaoException {
		tagService.delete(Long.valueOf(7));
		verify(tagDao).delete(anyLong());
	}

	@Test(expected = ServiceException.class)
	public void deleteFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).delete(anyLong());
		tagService.delete(Long.valueOf(4));
	}

	private void assertEntityEquals(TagEntity entity, TagEntity actualEntity) {
		assertEquals(entity.getName(), actualEntity.getName());
	}
}
